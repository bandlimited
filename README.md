bandlimited
===========

bandlimited wavetables for pd vanilla
<https://code.mathr.co.uk/bandlimited>


Usage
-----

See `bl-example.pd`.


Concept
-------

The idea is much the same as mipmapping (commonly used in OpenGL):
<https://en.wikipedia.org/wiki/Mipmap>

`[bl-table]` contains several tables at different resolutions (powers of
two with extra guard points for `[tabread4~]` interpolation).

`[bl-tabread4~]` uses the rate of change of the input phase to choose an
appropriate resolution to use.

`[bl-gen~]` generates the wavetable contents from a source table, by
iteratively filtering and downsampling to prevent aliasing.


Implementation
--------------

Each wave table should only be played slower than 1:1 table sample per
output sample.  The rate of change of the input phase determines the
resolution level to use:

    dphase = wrap(newphase - oldphase)
    delta = min(dphase, 1 - dphase)
    level = -log2(delta)

This level is fractional.  The wavetable at ceil(level) would be played
faster than 1:1 (causing aliasing), so interpolate between wavetables at
floor(level-1) and floor(level); level-1 is between these values.

Generating the tables uses FFT filtering, the gain is 1 between 0 and
SR/4, followed by a linear ramp to 0 between SR/4 and SR/2.  No window
is necessary, because the waveform is periodic with 1 period in the
block.


Legal
-----

bandlimited (c) 2015,2017 Claude Heiland-Allen

License: same as Pd vanilla

The following terms (the "Standard Improved BSD License") apply to all
files associated with the software unless explicitly disclaimed in
individual files:

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.


-- 
claude@mathr.co.uk
https://mathr.co.uk
